# Simple Sass Grid
### *v2.0.0*
*Light weight grid system.*  
Total compressed size:&nbsp;&nbsp;&nbsp;~*2kb*
***
__Darryn Bourgeois__   
Apr 1, 2016  
MIT
***
### How to use(*defaults*)

#### Setting Maps
1. __$grid__  
  * **use-prefix**:  
  { default: false }  
  { type: *BOOL* }  
  [ *ENABLE for the use of class prefix* ]
  * **prefix**:  
  { default: ssg }  
  { type: *STRING* }  
  [ *IF use-prefix enabled, prefix this to all classes generated* ]
  * **separator**:  
  { default: - }  
  { type: *STRING* }  
  [ *Separator for classes. Used for class prefixes* ]
  * **responsive**:  
  {default: true}  
  {type: *BOOL*}  
  [ *Set whether the grid is responsive or not* ]
2. __$container__
  * **name**:  
   { default: container }   
   { type: *STRING* }  
   [ *Set the name of container class* ]
3. __$row__
  * **name**:  
  { default: row }  
  { type: *STRING* }  
  [ *Set the name of row class* ]
4. __$columns__
  * **name**:  
  { type: *STRING* }  
  [ *Set the name of individual column class* ]  
  [ *Used for breakpoint() mixin* ]
    * **count**:  
    { type: *NUMBER* }  
    [ *OPTIONAL* ]  
    [ *Set the number of columns for this breakpoints classes* ]  
    [ *Used with prefix to generate classes* ]
    * **prefix**:  
    { type: *STRING* }  
    [ *OPTIONAL* ]  
    [ *Set the prefix for this breakpoints classes* ]  
    [ *Used with count to generate classes* ]
    * **breakpoint**:  
    { type: *NUMBER* or *STRING* }  
    [ *OPTIONAL* ]  
    [ *Set the breakpoint to be used* ]  
    [ *Used with count & prefix to generate classes* ]  
    [ *Used by self for breakpoint() mixin* ]  
5. __$gutters__  
  * **column**:  
  { type: *NUMBER* }  
  [ *Set the gutter width for columns* ]
  * **row**:  
  { type: *NUMBER* }  
  [ *Set the gutter height for rows* ]
