// Declare dependancies

var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');

// Individual Tasks

gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: 'presentation/' // browserSync root folder
    },
  })
})

gulp.task('sass', function() {
  return gulp.src('sass/**/*.scss') // Get sass files
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(sass({outputStyle: 'expanded', precision: 6}))
    .pipe(autoprefixer({browsers: ['last 2 versions', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'], cascade: true}))
    .pipe(browserSync.reload({stream: true}))
    .pipe(gulp.dest('presentation/css/')) // Destination compiles css
});

gulp.task('presentation', function() {
  return gulp.src('presentation/*.scss') // Get sass files
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(sass({outputStyle: 'expanded'}))
    .pipe(autoprefixer({browsers: ['last 2 versions', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'], cascade: true}))
    .pipe(browserSync.reload({stream: true}))
    .pipe(gulp.dest('presentation/css/')) // Destination compiles css
});


// Global Run Commands

gulp.task('default', ['browserSync', 'sass'], function() {
    gulp.watch('sass/*.scss', ['sass']); // Watch Sass, reload browserSync
    gulp.watch('sass/**/*.scss', ['sass']); // Watch Sass, reload browserSync
    gulp.watch('presentation/*.scss', ['presentation']); // Watch Presentational, reload browserSync
    gulp.watch('presentation/index.html', browserSync.reload); // Watch HTML, Reload browserSync
});
